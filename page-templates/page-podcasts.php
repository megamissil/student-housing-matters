<?php
/**
 * Template Name: Podcasts
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="page-home" role="main">
	<section class="blog-widgets">
		<div class="row">
			<div class="medium-10 medium-centered columns">
				<?php dynamic_sidebar( 'blog-widgets' ); ?>
			</div>
		</div>
	</section>
	<article class="main-content">
		<?php
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		$args = array ( 
			'posts_per_page' => 10, 
			'paged' => $paged,
            'tax_query' => array(
              	array(
                	'taxonomy' => 'category',
                	'field' => 'slug',
                	'terms' => 'join-the-conversation-podcast'
              	)
            )
		);
		query_posts($args);
		while ( have_posts() ) : the_post(); ?>
			<div class="row">
				<div class="medium-10 medium-centered columns">
					<?php get_template_part( 'template-parts/content', get_post_format() ); ?>
				</div>
			</div>
		<?php endwhile; ?>

		<div class="row medium-6 medium-centered columns">
			<?php the_posts_pagination( array(
				//'mid_size'  => 2,
				'prev_text' => __( 'Previous Page', 'textdomain' ),
				'next_text' => __( 'Next Page', 'textdomain' ),
			) ); ?>
		</div>

	</article>

</div>

<?php get_footer();
