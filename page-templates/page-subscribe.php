<?php
/**
 * Template Name: Subscribe
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="page-subscribe" role="main">
	<section class="blog-widgets">
		<div class="row">
			<div class="medium-10 medium-centered columns">
				<?php dynamic_sidebar( 'blog-widgets' ); ?>
			</div>
		</div>
	</section>

 	<?php while ( have_posts() ) : the_post(); ?>
		<article class="main-content">
			<div class="row">
				<div class="medium-10 medium-centered text-center columns">
					<h2><?php the_title(); ?></h2>
					<hr>
				</div>
			</div>
			<div class="row">
				<div class="medium-10 large-6 medium-centered columns text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</article>
	 <?php endwhile;?>
</div>

<?php get_footer();
