<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

		</section>
		<div class="header-widgets hide-for-medium">
			<div class="row">
				<?php dynamic_sidebar( 'header-widgets' ); ?>
			</div>
		</div>
		<div id="footer-container">
			<footer id="footer">
				<div class="hide-for-medium small-top-footer">
					<div class="row">
						<ul class="social-links">
							<li><a href="https://www.facebook.com/StudentHousingMatters/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
							<li><a href="https://twitter.com/OnCampusHousing" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
							<li><a href="https://www.linkedin.com/company/capstone-on-campus-management" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
							<li><a href="http://directory.dev.libsyn.com/shows/view/id/jointheconversation" target="_blank"><i class="fa fa-rss-square" aria-hidden="true"></i></a></li>
						</ul>
					</div>
					<div class="row nav-links">
						<a href="http://cocm.com/about/" target="_blank">about</a> | <a href="http://cocm.com/resources/">contact</a>
					</div>
				</div>
				<div class="row">
					<div class="medium-6 columns copy">
						<p>Copyright &copy; <?=date('Y'); ?> Student Housing Matters.</p>
					</div>
					<div class="medium-6 columns moxy">
						<a href="http://digmoxy.com" target="_blank">Moxy</a>
					</div>
				</div>
			</footer>
		</div>

		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) == 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/components/slick/slick/slick.min.js"></script>

<?php wp_footer(); ?>

<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
