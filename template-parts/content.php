<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class('blogpost-entry'); ?>>
	<div class="row blog-item">
		<div class="medium-4 columns">
			<?php if ( has_post_thumbnail() ) { ?>
   				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail('archive-img'); ?>
				</a>
			<?php } ?>
		</div>
		<div class="medium-8 columns">
			<p class="post-date"><?php the_date('F j, Y'); ?></p>
			<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
			<hr />
			<div class="row">
				<div class="medium-8 large-9 columns">
					<?php the_excerpt(); ?>
				</div>
				<div class="medium-4 large-3 columns">
					<a href="<?php the_permalink(); ?>" class="button read-more">Read More</a>
				</div>
			</div>
		</div>
	</div>
</div>
