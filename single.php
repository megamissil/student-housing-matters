<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<section class="blog-widgets">
	<div class="row">
		<div class="medium-10 medium-centered columns">
			<?php dynamic_sidebar( 'blog-widgets' ); ?>
		</div>
	</div>
</section>

<div id="single-post" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
		<div class="row single-content">
			<div class="medium-10 medium-centered columns">
				<h3><?php the_title(); ?></h3>

				<div class="entry-content">
					<?php if ( in_category( 'join-the-conversation-podcast' )) { ?>
						<div class="mp3-player">
							<?php if (types_render_field('podcast-link', array('output'=>'true'))) {
			              		$fileurl = types_render_field('podcast-link', array('raw'=>'true'));

								echo do_shortcode('[audio mp3="' . $fileurl .'" ogg="audio.ogg" wav="audio.wav" autoplay="off" loop="on" preload="on" controls="on" flash_fallback="on" hidden="off"]');
							} ?>
						</div>

						<?php the_content(); ?>
					<?php } else { ?>
						<?php the_content(); ?>
					<?php }  ?>
					
					<div class="row">
						<div class="medium-10 medium-centered columns">
							<?php if (types_render_field('post-image', array('output'=>'true'))) { ?>
								<div class="image-carousel">
									<div>
										<?=types_render_field('post-image', array('separator'=>'</div><div>')); ?>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-10 medium-centered columns">
				<div class="row single-nav">
					<div class="nav-previous medium-6 columns">
						<?php previous_post_link('%link', '<< Previous Post', TRUE); ?>
					</div>
					<div class="nav-next medium-6 columns">
						<?php next_post_link('%link', 'Next Post >>', TRUE); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="medium-6 medium-centered columns">
				<hr />
				<div class="row">
					<div class="medium-10 medium-centered columns author-info">
						<h5><?php the_author_posts_link(); ?></h5>
						<p><em><?php the_author_meta('description'); ?></em></p>
					</div>
				</div>
			</div>
		</div>
	</article>
<?php endwhile;?>

<?php do_action( 'foundationpress_after_content' ); ?>
</div>
<?php get_footer();
